![](ss_fixvideolink2201.png)

Formats a video link using HTML tags. It can provide formatted code for either a URL or a clickable image. Additional options are available only for the URL mode.

The application uses the:

- curl library by Daniel Stenberg	
	- https://github.com/curl/curl
	- https://curl.se/docs/copyright.html

- curl library AHK wrapper by RaptorX [GPL v3]	
	- https://github.com/RaptorX/cURL-Wrapper
	- https://www.gnu.org/licenses/gpl-3.0.txt

***

The curl license:

COPYRIGHT AND PERMISSION NOTICE

Copyright (c) 1996 - 2022, Daniel Stenberg, daniel@haxx.se, and many contributors, see the THANKS file.

All rights reserved.

Permission to use, copy, modify, and distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not be used in advertising or otherwise to promote the sale, use or other dealings in this Software without prior written authorization of the copyright holder.

***

The FixVideoLink code is distributed under the GPL license, whatever version suits you best.

Notes:

- The precompiled executable is 32bit only, but it should run fine in a 64bit environment.
- Currently there is only a 32bit version of the libcurl library being shipped with the package;
	should one want to compile and/or run the AHK code fully under 64bit AHK then the 64bit of the library
	should be downloaded, placed in the main folder, and appropriate changes should be made in the AHK
	code in regard to new library's file name.
- The FixVideoLink package bundles an older version of the libcurl library - namely 7.52.1 - because
	newer/latest versions are ten times (!!!) larger while bringing no benefit whatsoever to the project.
	One can always manually replace the built-in library (which is automatically deployed at first start)
	with a newer version. Following links should always provide the very latest versions of cURL and libcurl:		
32bit - https://curl.se/windows/latest.cgi?p=win32-mingw.zip		
64bit - https://curl.se/windows/latest.cgi?p=win64-mingw.zip
- Please do not mix 64bit code/AHK with 32bit libcurl.

**© Drugwash, 2017-2023**
